//
//  ViewController.swift
//  MapKitDemo
//
//  Created by iOS_Razrab on 22/09/2017.
//  Copyright © 2017 iOS_Razrab. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController {

    var mapView = MKMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        mapView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
        // --- надо иметь ввиду, из уроков географии ------------//
        // --- что широта имеет диапазон ------------------------//
        // --- -90 до +90 ---------------------------------------//
        // --- долгота от -180 до +180 --------------------------//
        
        let coord = CLLocationCoordinate2DMake(60, 20)
        
        let span = MKCoordinateSpanMake(1, 1)
        mapView.region = MKCoordinateRegionMake(coord, span)
        self.view.addSubview(mapView)
        
        /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
        
        
        // start: --- сделаем основную точку по центру экрана ↓
        
        let annotationMain = MKPointAnnotation()
        annotationMain.coordinate = coord
        annotationMain.title = "Main Name"
        self.mapView.addAnnotation(annotationMain)
        
        let viewPoint = mapView.convert(coord, toPointTo: self.view)
        let coordPoint = mapView.convert(viewPoint, toCoordinateFrom: self.view)
        print("Координты = \(coord) соответсвует точке view point на экране  = \(viewPoint)")
        
        // end: --- сделаем основную точку по центру экрана   ↑
        
        
        // --- добавим действие при нажатии на карту
        
        let uilpgr = UILongPressGestureRecognizer(target: self, action: #selector(longpress(gestureRecognizer:)))
        uilpgr.minimumPressDuration = 1
        mapView.addGestureRecognizer(uilpgr)
        
    }
    
    
    // start: --- создаем новую булавку, транслируем ее координаты как координаты экрана
    
    func longpress(gestureRecognizer: UIGestureRecognizer) {
        
        if gestureRecognizer.state == UIGestureRecognizerState.began {
            
            let touchPoint = gestureRecognizer.location(in: self.mapView)
            let newCoordinate = self.mapView.convert(touchPoint, toCoordinateFrom: self.mapView)
            let location = CLLocation(latitude: newCoordinate.latitude, longitude: newCoordinate.longitude)
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
                
                if error != nil {
                    print(error)
                } else {
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = newCoordinate
                    annotation.title = "New pin"
                    self.mapView.addAnnotation(annotation)
                    let newViewPoint = self.mapView.convert(newCoordinate, toPointTo: self.view)
                    print("Координты новой булавки = \(newCoordinate) соответсвует точке view point на экране  = \(newViewPoint)")
                }
                
            })
            
        }
        
    } // end: --- создаем новую булавку, транслируем ее координаты как координаты экрана


}
